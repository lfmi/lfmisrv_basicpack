##LFMI Server Installation##

```
apt-add-repository ppa:ansible/ansible
apt install ansible
apt install git
git clone https://gitlab.epfl.ch/lfmi/lfmisrv_basicpack.git
git clone https://gitlab.epfl.ch/sti-cluster/ansible_ldap_authentication.git
git clone https://gitlab.epfl.ch/sti-cluster/ansible_matlab.git
git clone https://gitlab.epfl.ch/sti-cluster/ansible_comsol.git
cd lfmisrv_basicpack/
ansible-playbook playbook.yml
ansible-playbook playbook1.yml
vim /etc/sssd/sssd.conf 
vim /etc/sudoers
init 6 to restart
Create folders for Spare and Scratch
mkdir /spare 
mkdir /scratch 
# Mount Filesystem for /sdb1 and /sdc1
mount /dev/sdb1  /spare
mount /dev/sdc1  /scratch
# Installation of Matlab and Comsol needs STI Admins to install- encrypted vault
ansible-playbook playbook2.yml  --vault-password-file=../vault.txt
ansible-playbook playbook3.yml  --vault-password-file=../vault.txt
```



